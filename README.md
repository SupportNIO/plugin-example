## How To

---
1. Go to Custom preprocessor repository on BitBucket.
2. Clone the codes from the main branch on your machine and open the CustomPreprocessor.sln file to open the codes inside Visual Studio (2019 or later preferred).
3. Build the library package by right clicking on the project inside the solution explorer.
4. Open the build folder, the dll library file is now created and ready to use.
