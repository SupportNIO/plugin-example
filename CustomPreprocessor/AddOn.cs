﻿using ContactRoute;
using Nixxis.Client.Admin;

namespace Nixxis.CustomControls
{

    [AdminSave(SkipSave = true)]
    [DialogType(Type = "Nixxis.CustomControls.DlgCustomPreprocessorConfigure, CustomPreprocessor")]
    public class CustomPreprocessorConfig : BasePreprocessorConfig
    {
        // This is the type ("Nixxis.CustomControls.CustomPreprocessorConfig, CustomPreprocessor") to specify as "Editor URL" when creating the preprocessor in admin

        public CustomPreprocessorConfig(AdminObject parent)
            : base(parent)
        {
        }

        public CustomPreprocessorConfig(AdminCore core)
            : base(core)
        {
        }

        public string CustomConfig
        {
            get;
            internal set;
        }

        public override void DeserializeFromText(string text)
        {
            if (string.IsNullOrEmpty(text)) return;

            if (text.StartsWith("CustomValue")) CustomConfig = text.Split('=')[1];
        }

        protected override string SerializeToText()
        {
            return string.Format("CustomValue={0}", CustomConfig);
        }
    }
}
