﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using Nixxis.Client.Admin;

namespace Nixxis.CustomControls
{
    /// <summary>
    /// </summary>
    public partial class DlgCustomPreprocessorConfigure : Window, IPreprocessorConfigurationDialog
    {
        private static TranslationContext m_TranslationContext = new TranslationContext(nameof(DlgCustomPreprocessorConfigure));

        private static string Translate(string text)
        {
            return m_TranslationContext.Translate(text);
        }

        private string m_Settings = null;

        public DlgCustomPreprocessorConfigure()
        {
            if (System.Globalization.CultureInfo.CurrentCulture.TextInfo.IsRightToLeft) FlowDirection = FlowDirection.RightToLeft;
            else FlowDirection = FlowDirection.LeftToRight;

            InitializeComponent();
        }

        public string Settings
        {
            get { return m_Settings; }
            set
            {
                m_Settings = value;
                LoadSettings();
            }
        }
        private enum KeywordDetectType
        {
            Subject,
            Body,
            Both
        }

        private const int MAX_ALLOWED_PROCESSOR = 10;
        private const char NEW_CUSTOM_PARAM_SEPARATOR = ';';
        private const char NEW_PARAM_ROW_SEPARATOR = '\n';

        private const string KEYWORD_CONTROLNAME = "txtKeywords";
        private const string QUEUE_CONTROLNAME = "cmbReplaceByQueueKeyword";
        private const string DETECT_IN_BODY_CONTROLNAME = "chkDetectInBody";
        private const string DETECT_IN_SUBJECT_CONTROLNAME = "chkDetectInSubject";
        private const string DETECT_IN_BOTH_CONTROLNAME = "chkDetectInBoth";

        private void TextBlock_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue) return;

            TextBlock tb = sender as TextBlock;

            StringBuilder sb = new StringBuilder();

            int iCount = 0;

            for (int i = 1; i <= MAX_ALLOWED_PROCESSOR; i++)
            {
                if ((this.FindName(KEYWORD_CONTROLNAME + i) is TextBox txtkey && txtkey != null && !string.IsNullOrEmpty(txtkey.Text))
                    && (this.FindName(QUEUE_CONTROLNAME + i) is ComboBox cmbQ && cmbQ != null && !string.IsNullOrEmpty(cmbQ.SelectedValuePath))
                    )
                {
                    iCount++;

                    sb.AppendFormat(Translate(Translate("List {0} :\n")), iCount);

                    if (this.FindName(DETECT_IN_SUBJECT_CONTROLNAME + i) is RadioButton rbSub && rbSub != null && rbSub.IsChecked.Value)
                    {
                        sb.AppendFormat(Translate("When {0} includes : {1}\n"), KeywordDetectType.Subject.ToString(), txtkey.Text);
                    }
                    else if (this.FindName(DETECT_IN_BODY_CONTROLNAME + i) is RadioButton rbBody && rbBody != null && rbBody.IsChecked.Value)
                    {
                        sb.AppendFormat(Translate("When {0} includes : {1}\n"), KeywordDetectType.Body.ToString(), txtkey.Text);
                    }
                    else if (this.FindName(DETECT_IN_BOTH_CONTROLNAME + i) is RadioButton rbboth && rbboth != null && rbboth.IsChecked.Value)
                    {
                        sb.AppendFormat(Translate("When {0} includes : {1}\n"), KeywordDetectType.Both.ToString(), txtkey.Text);
                    }

                    sb.AppendFormat(Translate("Queue -> {0}\n"), cmbQ.Text);
                    sb.AppendFormat("\n");
                }
            }

            if (sb.Length > 0) sb.AppendFormat(Translate("Otherwise default activity queue is selected."));

            tb.Text = sb.ToString();
        }

        private void WizControl_WizardFinished(object sender, RoutedEventArgs e)
        {
            SaveSettings();
            DialogResult = true;
        }

        private void LoadDropDowns()
        {
            GeneralSettings gen_settings = new GeneralSettings((IMainWindow)Application.Current.MainWindow);
            for (int i = 1; i <= MAX_ALLOWED_PROCESSOR; i++)
            {
                if (this.FindName(QUEUE_CONTROLNAME + i) is ComboBox cmbQ && cmbQ != null)
                {
                    cmbQ.ItemsSource = null;

                    if (DataContext is AdminCore ac && ac != null)
                    {
                        if (gen_settings != null && gen_settings.IsFullVersion)
                        {
                            cmbQ.ItemsSource = new ExtendedEnumerableNoDummies(ac.Queues);
                        }
                        else
                        {
                            cmbQ.ItemsSource = new ExtendedEnumerable(ac.Queues);
                        }
                    }
                }
            }
        }

        public void SaveSettings()
        {
            if (Config == null) m_Config = ((AdminObject)WizControl.Context).Core.Create<CustomPreprocessorConfig>();

            StringBuilder sb = new StringBuilder();

            for (int i = 1; i <= MAX_ALLOWED_PROCESSOR; i++)
            {
                if ((this.FindName(KEYWORD_CONTROLNAME + i) is TextBox txtkey && txtkey != null && !string.IsNullOrEmpty(txtkey.Text))
                    && (this.FindName(QUEUE_CONTROLNAME + i) is ComboBox cmbQ && cmbQ != null && !string.IsNullOrEmpty(cmbQ.SelectedValuePath))
                    )
                {
                    string detect_in = string.Empty;

                    if (this.FindName(DETECT_IN_SUBJECT_CONTROLNAME + i) is RadioButton rbSub && rbSub != null && rbSub.IsChecked.Value) detect_in = KeywordDetectType.Subject.ToString();
                    else if (this.FindName(DETECT_IN_BODY_CONTROLNAME + i) is RadioButton rbBody && rbBody != null && rbBody.IsChecked.Value) detect_in = KeywordDetectType.Body.ToString();
                    else if (this.FindName(DETECT_IN_BOTH_CONTROLNAME + i) is RadioButton rbboth && rbboth != null && rbboth.IsChecked.Value) detect_in = KeywordDetectType.Both.ToString();

                    sb.AppendFormat("{0}{1}{2}{1}{3}{4}", txtkey.Text.Trim(), NEW_CUSTOM_PARAM_SEPARATOR, detect_in, cmbQ.SelectedValue, NEW_PARAM_ROW_SEPARATOR);
                }
            }

            m_Config.CustomConfig = sb.ToString();
        }

        public void LoadSettings()
        {        
            LoadDropDowns();

            try
            {
                if (m_Config != null && m_Config.CustomConfig != null)
                {
                    string[] customdata = m_Config.CustomConfig.Split(NEW_PARAM_ROW_SEPARATOR);

                    if (customdata != null && customdata.Length > 0)
                    {
                        for (int i = 0; i < customdata.Length; i++)
                        {
                            if (string.IsNullOrEmpty(customdata[i])) break;
                            if (i >= MAX_ALLOWED_PROCESSOR) break;

                            string[] setparams = customdata[i].Split(NEW_CUSTOM_PARAM_SEPARATOR);

                            if (setparams != null && setparams.Length > 0)
                            {
                                if (this.FindName(KEYWORD_CONTROLNAME + (i + 1)) is TextBox txtDesc && txtDesc != null && !string.IsNullOrEmpty(setparams[0])) txtDesc.Text = setparams[0];
                                if (this.FindName(QUEUE_CONTROLNAME + (i + 1)) is ComboBox cmbQ && cmbQ != null && setparams.Length >= 2 && !string.IsNullOrEmpty(setparams[2])) cmbQ.SelectedValue = setparams[2];

                                if (setparams.Length >= 1 && !string.IsNullOrEmpty(setparams[1]) &&
                                    (setparams[1] == KeywordDetectType.Body.ToString() || setparams[1] == KeywordDetectType.Subject.ToString() || setparams[1] == KeywordDetectType.Both.ToString()))
                                {
                                    if (this.FindName(DETECT_IN_SUBJECT_CONTROLNAME + (i + 1)) is RadioButton rbSub && rbSub != null && setparams[1] == KeywordDetectType.Subject.ToString()) rbSub.IsChecked = true;
                                    if (this.FindName(DETECT_IN_BODY_CONTROLNAME + (i + 1)) is RadioButton rbBody && rbBody != null && setparams[1] == KeywordDetectType.Body.ToString()) rbBody.IsChecked = true;
                                    if (this.FindName(DETECT_IN_BOTH_CONTROLNAME + (i + 1)) is RadioButton rbboth && rbboth != null && setparams[1] == KeywordDetectType.Both.ToString()) rbboth.IsChecked = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("onload:: " + ex.Message);
            }
        }

        public object WizControlContext
        {
            get { return WizControl.Context; }
            set { WizControl.Context = value; }
        }

        private CustomPreprocessorConfig m_Config = null;

        public BasePreprocessorConfig Config
        {
            get { return m_Config; }
            set
            {
                if (value is CustomPreprocessorConfig) m_Config = (CustomPreprocessorConfig)value;

                LoadSettings();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cmbReplaceByQueueKeyword_DropDownOpened(object sender, System.EventArgs e)
        {

        }

        private void cmbReplaceByQueueKeyword_DropDownClosed(object sender, System.EventArgs e)
        {

        }
    }
}
